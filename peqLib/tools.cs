﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;
using System.Reflection;

namespace peqMulti
{
    public enum peopleType { STUDENT = 1, TEACHER = 2 };
    public enum evalLevel { NA = 0, WAIT = 1, OK = 2, ONGOING = 3, NOTOK = 4 };
    public enum testOptions { MIXED = 1, SMALL, BIG, REVERSE }
    public enum resultType { NONE = 0, TESTTABLE = 1, TESTSTUDENT = 2, CLASSTABLE = 3, CLASSSTUDENT = 4 };
    public enum aggregationOption { STATONETABLE = 1, SPLITPERTABLE = 2, SUMALLTABLES = 3 };
    public enum col1Type { TABLE = 1, STUDENT = 2 };
    public class config
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
    public class param
    {
        public SqlConnection conn = new SqlConnection();
        public Int32 firstTimer = 7;
        public Int32 SecondTimer = 30;

        public static param me = new param();

        private void loadIfNull(config sqlConf, string key, string defaut)
        {
            PropertyInfo pr = sqlConf.GetType().GetProperty(key);
            if (pr.GetValue(sqlConf, null) == null)
            {
                pr.SetValue(sqlConf, defaut);
            }
        }
        private param()
        {
            config sqlConf = this.loadCxionString();

            conn = new SqlConnection(
                "Data Source=" + sqlConf.Server + ";" +
                "Initial Catalog=" + sqlConf.Database + ";" +
                "User Id=" + sqlConf.UserId + ";" +
                "Password=" + sqlConf.Password + ";");
        }
        public config loadCxionString()
        {
            config sqlCfg = new config();

            string json = "{\'Server\':\'.\\\\SQLEXPRESS\' }";

            if (System.IO.File.Exists("./config.json"))
            {
                using (StreamReader r = new StreamReader("./config.json"))
                {
                    json = r.ReadToEnd();
                }
            }
            sqlCfg = JsonConvert.DeserializeObject<config>(json);
            this.loadIfNull(sqlCfg, "Server", "CLASS-SRV\\SQLEXPRESS");
            this.loadIfNull(sqlCfg, "Database", "peqMulti");
            this.loadIfNull(sqlCfg, "UserId", "sa");
            this.loadIfNull(sqlCfg, "Password", "sasasasa");
            return sqlCfg;
        }
    }

    public class item
    {
        public Int32 id { get; set; }
        public string name { get; set; }
    }
    public class itemList<T> where T : item
    {
        protected List<T> all = new List<T>();
        public T get(string name)
        {
            foreach (T oneIt in all)
            {
                if (oneIt.name == name)
                    return oneIt;
            }
            return null;
        }
        public List<string> dataSource()
        {
            List<string> ds = new List<string>();
            ds.Add("");
            foreach (T oneIt in all)
                ds.Add(oneIt.name);
            return ds;
        }
    }

    public class oneTable : item
    {
        public Int32 startIdx;
        public Int32 endIdx;
    }
    public class allTables : itemList<oneTable>
    {
        public static allTables me = new allTables();
        private allTables()
        {
            all = new List<oneTable>();
            for (Int32 idx = 0; idx < 9; idx++)
            {
                oneTable ot = new peqMulti.oneTable();
                ot.name = "Table " + (idx + 1).ToString();
                ot.id = idx + 1;
                ot.startIdx = idx * 9;
                ot.endIdx = ot.startIdx + 8;
                all.Add(ot);
            }
        }
    }
    public class classe : item
    {
    }
    public class classes : itemList<classe>
    {
        public static classes me = new classes();

        private classes()
        {
        }
        public classe add(Int32 id, String name)
        {
            classe cl = new classe();
            cl.id = id;
            cl.name = name;
            all.Add(cl);
            return cl;
        }
    }

    public class person : item
    {
        public Int32 classNo;
        public peopleType type;
    }
    public class people : itemList<person>
    {
        public static people me = new people();
        private people()
        {
        }
        public person add(Int32 id, classe cl, string name, string type)
        {
            person p = new person();
            p.id = id;
            p.classNo = cl.id;
            p.name = name;
            try
            {
                p.type = (peopleType)Enum.Parse(typeof(peopleType), type);
            }
            catch
            {
                p.type = peopleType.STUDENT;
            }
            all.Add(p);
            return p;
        }
        public List<string> dataSourceTeacher()
        {
            List<string> studentName = new List<string>();
            studentName.Add("");
            foreach (person student in all)
            {
                if (student.type == peopleType.TEACHER)
                {
                    studentName.Add(student.name);
                }
            }
            return studentName;
        }
        public List<string> dataSourceStudents()
        {
            List<string> studentName = new List<string>();
            studentName.Add("");
            foreach (person student in all)
            {
                if (student.type == peopleType.STUDENT)
                {
                    studentName.Add(student.name);
                }
            }
            return studentName;
        }
    }

    public class test : item
    {
        public Int32 classNo { get; set; }
        public DateTime date { get; set; }
        public String tables;
        public testOptions options;
        public void updateSql()
        {
            string query = "update test set " +
                "name = '" + name + "', " +
                "date = '" + date.ToString("yyyy/MM/dd") + "', " +
                "tables = '" + tables + "', " +
                "options = " + options + " " +
                "where id = " + this.id.ToString() + ";";
            SqlCommand cmd = new SqlCommand(query, param.me.conn);
            cmd.ExecuteNonQuery();
        }
    }
    public class tests : itemList<test>
    {
        public static tests me = new tests();
        private tests()
        {
        }
        public test add(Int32 id, classe cl, string name, DateTime date, String tables, Int32 options)
        {
            test t = new test();
            t.id = id;
            t.classNo = cl.id;
            t.name = name;
            t.tables = tables;
            t.date = date;
            try
            {
                t.options = (testOptions)options;
            }
            catch
            {
                t.options = testOptions.MIXED;
            }
            all.Add(t);
            return t;
        }
        public void addSql(String name, DateTime date, String className, String tables, Int32 Options = 0)
        {
            classe cls = classes.me.get(className);
            string query = "insert into test(name, date, classId, tables, options) Values (" +
                "'" + name + "', '" + date.ToString("yyyy/MM/dd") + "', " + cls.id + ", '" + 
                tables + "', " + Options + "); SELECT CAST(scope_identity() AS int)";
            SqlCommand cmd = new SqlCommand(query, param.me.conn);
            Int32 id = (Int32)cmd.ExecuteScalar();
            add(id, cls, name, date, tables, Options);
        }
        public List<test> getDataSourceForAClass(Int32 classId)
        {
            List<test> rslt = new List<test>();
            foreach (test tst in all)
            {
                if (tst.classNo == classId)
                    rslt.Add(tst);
            }
            return rslt;
        }
    }
    public class result : item
    {
        public Int32 testId;
        public Int32 personId;
        public List<Boolean> tables;
        public String tablesSql;
        public List<Boolean> tableToBeWorked;
        public List<List<evalLevel>> eval;
        public String evalSql;
        public DateTime date;

        public bool isDone()
        {
            computeTableToBeWorked();
            for (Int32 i = 0; i < 9; i++)
            {
                if (tableToBeWorked[i])
                    return false;
            }
            return true;
        }

        public List<Int32> getNewTest()
        {
            int alea = 0;
            int idx = 0;
            int nbTestRemain = 0;
            List<Int32> nextTest = new List<Int32>();
            nextTest.Add(-1);
            nextTest.Add(-1);
            // check if there is still some work to be done
            while (idx < 9)
            {
                if (tableToBeWorked[idx])
                    alea++;
                for (Int32 itm = 0; itm < 9; itm++)
                    if (eval[idx][itm] == evalLevel.WAIT)
                        nbTestRemain++;
                idx++;
            }
            if (alea == 0)
                return nextTest;

            // let find the new test
            alea = new Random(DateTime.Now.Millisecond).Next(nbTestRemain);
            if (alea == 0)
                alea = 1;
            idx = alea;
            while (alea-- >= 0)
            {
                if ((idx % 9) < 2)
                {
                    idx++;
                    continue;
                }
                nextTest[1] = idx % 9;
                nextTest[0] = (idx - nextTest[1]) / 9;
                while (eval[nextTest[0]][nextTest[1]] != evalLevel.WAIT)
                {
                    idx++;
                    if (idx >= 81)
                        idx = 0;
                    nextTest[1] = idx % 9;
                    nextTest[0] = (idx - nextTest[1]) / 9;
                }
                idx++;
                if (idx >= 81)
                    idx = 0;
            }
            // convert to number from 1 to 9
            nextTest[0]++;
            nextTest[1]++;
            return nextTest;
        }
        public void loadFromTest(test t, person p)
        {
            id = 0;
            testId = t.id;
            personId = p.id;
            tables = new List<bool>();
            tablesSql = "";
            eval = new List<List<evalLevel>>();
            evalSql = "";
            name = t.name + "-" + date.ToString("yy-MM-dd");
            for (int i = 0; i < 9; i++)
            {
                eval.Add(new List<evalLevel>());
                int idxEval = eval.Count - 1;
                tables.Add(t.tables.Substring(i, 1) != " ");
                evalLevel ev = evalLevel.WAIT;
                if (!tables[tables.Count - 1])
                    ev = evalLevel.NA;
                for (Int32 ii = 0; ii < 9; ii++)
                    eval[idxEval].Add(ev);
            }
            date = DateTime.Now;
            loadFromInternal();
            computeTableToBeWorked();
        }
        private void loadFromSql()
        {
            tables = new List<bool>();
            eval = new List<List<evalLevel>>();
            for (int i = 0; i < 9; i++)
            {
                if (tablesSql.Substring(i, 1) == " ")
                    tables.Add(false);
                else
                    tables.Add(true);
                eval.Add(new List<evalLevel>());
                for (Int32 j = 0; j < 9; j++)
                {
                    if (evalSql.Substring(i * 9 + j, 1) == " ")
                        eval[i].Add(evalLevel.NA);
                    else
                        eval[i].Add((evalLevel)Enum.Parse(typeof(evalLevel), evalSql.Substring(i * 9 + j, 1)));
                }
            }
            computeTableToBeWorked();
        }
        private void loadFromInternal()
        {
            tablesSql = "";
            evalSql = "";
            for (int i = 0; i < 9; i++)
            {
                if (tables[i])
                    tablesSql += "X";
                else
                    tablesSql += " ";
                for (Int32 j = 0; j < 9; j++)
                    evalSql += (int)eval[i][j];
            }
        }
        public void updateDB()
        {
            loadFromInternal();
            string query = " update results set eval = '" + evalSql + "' where id = " + id.ToString();
            SqlCommand cmd = new SqlCommand(query, param.me.conn);
            cmd.ExecuteNonQuery();
        }
        public void loadDB(classe c, test t, person p)
        {
            if (p == null || t == null || c == null)
            {
                throw new Exception("a class/test/personn is not valid");
            }
            string query = "select id, tables, date, eval from results where peopleId = " + p.id + " and testId = " + t.id;
            SqlCommand cmd = new SqlCommand(query, param.me.conn);
            cmd.CommandText = query;
            SqlDataReader drd = cmd.ExecuteReader();
            if (!drd.HasRows)
            {
                loadFromTest(t, p);
                query = " insert into results(testId, peopleId, tables, date, eval) Values (" + testId.ToString() + ", " + personId.ToString() + ", '" + tablesSql + "', '" + date.ToString("yyyy/MM/dd") + "', '" + evalSql + "');";
                cmd = new SqlCommand(query, param.me.conn);
                drd.Close();
                cmd.ExecuteNonQuery();
                query = "select id, tables, date, eval from results where peopleId = " + p.id + " and testId = " + t.id;
                cmd = new SqlCommand(query, param.me.conn);
                cmd.CommandText = query;
                drd = cmd.ExecuteReader();
            }
            while (drd.Read())
            {
                id = Int32.Parse(drd["id"].ToString());
                date = DateTime.Parse(drd["date"].ToString());
                testId = t.id;
                personId = p.id;
                tables = new List<bool>();
                tablesSql = drd["tables"].ToString();
                eval = new List<List<evalLevel>>();
                evalSql = drd["eval"].ToString();
                loadFromSql();
                computeTableToBeWorked();
            }
            drd.Close();
        }
        public void setEval(List<Int32> testToBeDone, evalLevel newVal)
        {
            Int32 row = testToBeDone[0];
            Int32 col = testToBeDone[1];
            eval[row - 1][col - 1] = newVal;
            computeTableToBeWorked();
        }
        private void computeTableToBeWorked()
        {
            tableToBeWorked = new List<bool>();
            for (Int32 i = 0; i < 9; i++)
            {
                bool bThisTable = true;
                if (tables[i])
                {
                    for (Int32 j = 0; j < 9; j++)
                    {
                        if (eval[i][j] == evalLevel.WAIT)
                        {
                            bThisTable = false;
                            break;
                        }
                    }
                }
                if (tables[i] && !bThisTable)
                    tableToBeWorked.Add(true);
                else
                    tableToBeWorked.Add(false);
            }
        }
    }
    public class itemResult
    {
        public string peopleName;
        public string className;
        public string testName;
        public List<Boolean> tables;
        public String tablesSql;
        public List<List<evalLevel>> eval;
        public String evalSql;
        public DateTime date;

        public itemResult(string className, string peopleName, string testName, string tables, string eval, DateTime date)
        {
            this.peopleName = peopleName;
            this.className = className;
            this.testName = testName;
            this.tablesSql = tables;
            this.evalSql = eval;
            this.date = date;
            loadFromSql();
        }
        private void loadFromSql()
        {
            tables = new List<bool>();
            eval = new List<List<evalLevel>>();
            for (int i = 0; i < 9; i++)
            {
                if (tablesSql.Substring(i, 1) == " ")
                    tables.Add(false);
                else
                    tables.Add(true);
                eval.Add(new List<evalLevel>());
                for (Int32 j = 0; j < 9; j++)
                {
                    if (evalSql.Substring(i * 9 + j, 1) == " ")
                        eval[i].Add(evalLevel.NA);
                    else
                        eval[i].Add((evalLevel)Enum.Parse(typeof(evalLevel), evalSql.Substring(i * 9 + j, 1)));
                }
            }
        }
    }
    public class resultAll
    {
        private List<itemResult> all;
        public static resultAll me = new resultAll();
        Dictionary<string, List<int>> tmpResult = new Dictionary<string, List<int>>();
        Dictionary<string, DateTime> tmpRDate = new Dictionary<string, DateTime>();
        public List<string> colHdr;

        private resultAll()
        {
            all = new List<itemResult>();
            colHdr = new List<string>();
            for (Int32 ii = 0; ii < 9; ii++)
                colHdr.Add(ii.ToString());
        }
        public void loadFromDB()
        {
            all = new List<itemResult>();
            string query = "select p.name as peopleName, t.name as testName, c.name as className, r.tables as tables, r.eval as eval, r.date as date " +
                "from results r join people p on r.peopleId = p.id join class c on c.id = p.classId join test t on t.id = r.testId; ";

            SqlCommand cmd = new SqlCommand(query, param.me.conn);
            cmd.CommandText = query;
            SqlDataReader drd = cmd.ExecuteReader();
            Dictionary<String, List<Int32>> tmpResult = new Dictionary<string, List<int>>();
            while (drd.Read())
            {
                String peopleName = drd["peopleName"].ToString();
                String testName = drd["testName"].ToString();
                String className = drd["className"].ToString();
                String tables = drd["tables"].ToString();
                String oneEval = drd["eval"].ToString();
                DateTime date = DateTime.Parse(drd["date"].ToString());

                itemResult ir = new itemResult(className, peopleName, testName, tables, oneEval, date);
                all.Add(ir);
            }
            drd.Close();
        }
        private col1Type getCol1Type(string className, string peopleName, string tableName, string testName, resultType rsType)
        {
            col1Type ct = col1Type.TABLE;

            if (//rsType == resultType.TESTTABLE && tableName != null ||
                rsType == resultType.TESTSTUDENT && peopleName == null ||
                rsType == resultType.CLASSSTUDENT && peopleName == null)
                ct = col1Type.STUDENT;

            colHdr = new List<string>();
            switch (ct)
            {
                case col1Type.STUDENT:
                    for (Int32 ii = 0; ii < 9; ii++)
                        colHdr.Add("t " + (ii + 1).ToString());
                    break;
                case col1Type.TABLE:
                    for (Int32 ii = 0; ii < 9; ii++)
                        colHdr.Add((ii + 1).ToString() + "X...");
                    break;
            }
            return ct;
        }
        private List<bool> hackTables(string tableName)
        {
            Int32 idx = -1;
            List<bool> hackTables = new List<bool>();
            idx = Int32.Parse(tableName.Substring(5));
            if (idx < 1 || idx > 9)
                throw new Exception("invalid table name: " + tableName);
            for (Int32 ii = 0; ii < 9; ii++)
            {
                if ((ii + 1) == idx)
                    hackTables.Add(true);
                else
                    hackTables.Add(false);
            }
            return hackTables;
        }
        public void compute(
            string className, string peopleName, string tableName, string testName, string tables, aggregationOption aggreOpt, resultType rsType,
            bool addYes, bool addOnGoing, bool addNo,
            bool bLastOnly = false)
        {
            List<bool> myTables;
            dispResults dr = dispResults.me;
            dr.reset();

            col1Type col1Tp = getCol1Type(className, peopleName, tableName, testName, rsType);

            foreach (itemResult ir in all)
            {
                myTables = ir.tables;
                if (className != null && className != ir.className)
                    continue;
                if (peopleName != null && peopleName != ir.peopleName)
                    continue;
                if (testName != null && testName != ir.testName)
                    continue;
                if (tableName != null)
                    myTables = hackTables(tableName);

                string col1 = ir.peopleName;
                if (col1Tp == col1Type.TABLE)
                    col1 = "Table 1";

                for (Int32 i = 0; i < 81; i++)
                {
                    Int32 tab2 = i % 9;
                    Int32 tab1 = (i - tab2) / 9;

                    if (!myTables[tab1])
                        continue;

                    Int32 idStat = i;
                    if (aggreOpt == aggregationOption.SUMALLTABLES)
                        idStat = tab1;

                    if (rsType == resultType.TESTTABLE && tableName != null ||
                        rsType == resultType.CLASSTABLE && tableName != null)
                    {
                        col1 = ir.peopleName;
                        idStat = tab2;
                    } else if (col1Tp == col1Type.TABLE)
                    {
                        col1 = "Table " + (tab1 + 1).ToString();
                        idStat = tab2;
                    }

                    switch (ir.eval[tab1][tab2])
                    {
                        case evalLevel.OK:
                            if (addYes)
                                dispResults.me.addStat(col1, idStat);
                            else
                                dispResults.me.addStat(col1, idStat, 0, 1);
                            break;
                        case evalLevel.NOTOK:
                            if (addNo)
                                dispResults.me.addStat(col1, idStat);
                            else
                                dispResults.me.addStat(col1, idStat, 0, 1);
                            break;
                        case evalLevel.ONGOING:
                            if (addOnGoing)
                                dispResults.me.addStat(col1, idStat);
                            else
                                dispResults.me.addStat(col1, idStat, 0, 1);
                            break;
                    }
                }
            }
            dispResults.me.aggreAll();
        }
    }

    public class dispResult
    {
        public string col1 { get; set; }
        public Int32 t1 { get; set; }
        public Int32 t2 { get; set; }
        public Int32 t3 { get; set; }
        public Int32 t4 { get; set; }
        public Int32 t5 { get; set; }
        public Int32 t6 { get; set; }
        public Int32 t7 { get; set; }
        public Int32 t8 { get; set; }
        public Int32 t9 { get; set; }
        public Int32 count;
        private List<Int32> sumTableRst;
        public List<Int32> countTableRst;
        public dispResult()
        {
            sumTableRst = new List<int>();
            countTableRst = new List<int>();
            count = 0;
            t1 = 0; t2 = 0; t3 = 0; t4 = 0;
            t5 = 0; t6 = 0; t7 = 0; t8 = 0;
            t9 = 0;
            for (Int32 i = 0; i < 9; i++)
            {
                sumTableRst.Add(0);
                countTableRst.Add(0);
            }
        }
        public void addStat(Int32 idx, Int32 val = 1, Int32 count = 1)
        {
            sumTableRst[idx] += val;
            countTableRst[idx] += count;
        }
        public void runAggregation()
        {
            for (Int32 i = 0; i < 9; i++)
            {
                if (countTableRst[i] == 0)
                    sumTableRst[i] = -1;
                else
                {
                    sumTableRst[i] = sumTableRst[i] * 100/ countTableRst[i];
                    countTableRst[i] = 1;
                }
            }
            t1 = sumTableRst[0];
            t2 = sumTableRst[1];
            t3 = sumTableRst[2];
            t4 = sumTableRst[3];
            t5 = sumTableRst[4];
            t6 = sumTableRst[5];
            t7 = sumTableRst[6];
            t8 = sumTableRst[7];
            t9 = sumTableRst[8];
        }
    }
    public class dispResults
    {
        private List<dispResult> all = new List<dispResult>();
        public static dispResults me = new dispResults();

        private dispResults()
        {
            all = new List<dispResult>();
        }
        public void reset()
        {
            all = new List<dispResult>();
        }
        public dispResult get(String key)
        {
            foreach (dispResult dr in all)
            {
                if (dr.col1 == key)
                    return dr;
            }
            return null;
        }
        public void addStat(String key, Int32 idx, Int32 val = 1, Int32 count = 1)
        {
            dispResult dr = get(key);
            if (dr == null)
            {
                dr = new dispResult();
                dr.col1 = key;
                all.Add(dr);
            }
            dr.addStat(idx, val, count);
        }
        public void aggreAll()
        {
            foreach (dispResult dr in all)
                dr.runAggregation();
        }
        public List<dispResult> dataSource()
        {
            return all.OrderBy(r => r.col1).ToList();
        }
        public void add(dispResult dr)
        {
            all.Add(dr);
        }
        public void sort()
        {
            all.Sort();
        }
    }
}
