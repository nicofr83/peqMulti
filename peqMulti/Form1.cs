﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace peqMulti
{
    public partial class peqMultiplication : Form
    {
        public peqMultiplication()
        {
            InitializeComponent();
            loadData();
        }
        private void loadData()
        {
            try
            {
                cbClass.DataSource = null;
                string query = "select id, name from class";
                SqlCommand cmd = new SqlCommand(query, param.me.conn);
                cmd.CommandText = query;
                param.me.conn.Open();
                SqlDataReader drd = cmd.ExecuteReader();
                cbClass.Items.Clear();
                while (drd.Read())
                {
                    classes.me.add(Int32.Parse(drd["id"].ToString()), drd["name"].ToString());
                }
                drd.Close();
                cbClass.DataSource = classes.me.dataSource();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.ToString());
            }
        }

        private void cbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbClass.Text.Length == 0 && cbClass.SelectedIndex <= 0)
            {
                //cbPeople.Text = "";
                return;
            }
            try
            {
                classe cl = classes.me.get(cbClass.Text);
                cbPeople.DataSource = null;
                cbPeople.Items.Clear();
                if (cl == null)
                {
                    cbPeople.Text = "";
                    return;
                }
                // load people
                string query = "select id, name, type from people where classId = " + cl.id;
                SqlCommand cmd = new SqlCommand(query, param.me.conn);
                cmd.CommandText = query;
                SqlDataReader drd = cmd.ExecuteReader();
                while (drd.Read())
                {
                    people.me.add(Int32.Parse(drd["id"].ToString()),cl, drd["name"].ToString(), drd["type"].ToString());
                }
                drd.Close();
                cbPeople.DataSource = people.me.dataSourceStudents();

                // load tests for the class
                query = "select id, name, tables, date, options from test where classId = " + cl.id;
                cmd = new SqlCommand(query, param.me.conn);
                cmd.CommandText = query;
                drd = cmd.ExecuteReader();
                while (drd.Read())
                {
                    tests.me.add(Int32.Parse(drd["id"].ToString()), cl, drd["name"].ToString(), 
                        DateTime.Parse(drd["date"].ToString()), drd["tables"].ToString(), 
                        Int32.Parse(drd["options"].ToString()));
                }
                drd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.ToString());
            }

        }

        private void cbPeople_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPeople.Text.Length == 0)
            {
                //cbClass.Text = "";
                return;
            }
            try
            {
                cbTest.DataSource = null;
                person p = people.me.get(cbPeople.Text);
                if (p.type == peopleType.TEACHER)
                {
                    return;
                }
                cbTest.DataSource = tests.me.dataSource();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.ToString());
            }
        }

        private void cbTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTest.Text.Length == 0)
                return;
            person p = people.me.get(cbPeople.Text);
            test t = tests.me.get(cbTest.Text);
            classe c = classes.me.get(cbClass.Text);
            result r = new result();
            r.loadDB(c, t, p);
            peqStudent pqStudent = new peqStudent();
            if (r.isDone())
            {
                MessageBox.Show("Test terminated");
                return;
            }
            pqStudent.startEval(r);
            pqStudent.ShowDialog();
            pqStudent.Close();
        }

        private void peqMultiplication_Load(object sender, EventArgs e)
        {

        }
    }
}
