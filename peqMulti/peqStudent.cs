﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace peqMulti
{    public partial class peqStudent : Form
    {
        Boolean bPaused = false;
        result r;
        evalLevel resultEvalLevel = evalLevel.WAIT;
        Int32 challengeAnswer = -1;
        List<Int32> chal = new List<int>();

        public peqStudent()
        {
            InitializeComponent();
            prBar.Maximum = param.me.SecondTimer;
        }

        public void startEval(result _result)
        {
            r = _result;
            resultEvalLevel = evalLevel.NA;
            challengeNext();
        }

        public void challengeNext()
        {
            if (resultEvalLevel != evalLevel.NA)
                challengeAnswered();

            chal = r.getNewTest();
            if (chal[0] < 0)
            {
                challengeTerminated();
                return;
            }
            challengeAnswer = chal[0] * chal[1];
            txLeft.Text = chal[0].ToString();
            txRight.Text = chal[1].ToString();
            picLeft.Image = (Image)Properties.Resources.ResourceManager.GetObject("left" + chal[0].ToString());
            picRight.Image = (Image)Properties.Resources.ResourceManager.GetObject("right" + chal[1].ToString());
            txtResult.Text = "";
            prBar.ForeColor = Color.Green;
            prBar.Value = 0;
            resultEvalLevel = evalLevel.WAIT;
            timer1.Interval = 1000;
            timer1.Enabled = true;
            txtResult.Focus();
        }
        public void challengeAnswered()
        {
            timer1.Enabled = false;
            Int32 tmpResult = 0;
            try
            {
                tmpResult = Int32.Parse(txtResult.Text);
            } catch {; }
            if (tmpResult == challengeAnswer)
            {
                if (prBar.Value <= param.me.firstTimer)
                    resultEvalLevel = evalLevel.OK;
                else
                    resultEvalLevel = evalLevel.ONGOING;
            }
            else
            {
                resultEvalLevel = evalLevel.NOTOK;
            }
            r.setEval(chal, resultEvalLevel);
            txtInfo.ReadOnly = false;
            txtInfo.Text = chal[0].ToString() + " X " + chal[1].ToString() + " : = " + txtResult.Text + " is: " + resultEvalLevel.ToString() + "!!";
            txtInfo.ReadOnly = true;
            txtResult.Focus();
        }
        public void challengeTerminated()
        {
            r.updateDB();
            MessageBox.Show("Done !");
            this.Close();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            challengeNext();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            prBar.Value = prBar.Value + 1;
            Console.WriteLine("progressBar: " + prBar.Value);
            if (prBar.Value < param.me.firstTimer)
            {
                prBar.ForeColor = Color.Green;
                return;
            }
            if (prBar.Value < param.me.SecondTimer)
            {
                prBar.ForeColor = Color.Orange;
                prBar.Refresh();
                Refresh();
            }
            else
            {
                prBar.ForeColor = Color.Red;
                challengeNext();
            }
        }

        private void btQuit_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            if (MessageBox.Show("Do you want to really save your work, and exit this test", "Quit a test", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                r.updateDB();
                this.Close();
            }
            timer1.Start();
        }

        private void btPause_Click(object sender, EventArgs e)
        {
            if (bPaused)
            {
                btPause.Text = "Pause";
                timer1.Start();
            } else
            {
                btPause.Text = "Resume";
                timer1.Stop();
            }
            bPaused = !bPaused;
        }
    }
}
