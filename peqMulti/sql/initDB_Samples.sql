﻿create table class(id int not null identity primary key, name varchar(30))
go
create table people(id int not null identity primary key, classId int not null references class(id), name varchar(30), type char (1) not null)
go
create table test(id int not null identity primary key, name varchar(30), date dateTime default getDate(), classId int references class(id), tables text not null, options int not null default 0)
go

create table results(id int not null identity primary key, testId int references test(id), peopleId int references people(id), tables text not null, eval text not null, date dateTime default getDate())
go
insert into class(name) values('level 1')
insert into class(name) values('level 2')
insert into class(name) values('level 3')
go
insert into people(classId, name, type) values (1, 'toto', '1')
insert into people(classId, name, type) values (1, 'titi', '1')
insert into people(classId, name, type) values (1, 'tutu', '1')
insert into people(classId, name, type) values (1, 'prof', '2')
go
insert into test(name, classId, tables) values ('table 3', 1, '  X      ')
insert into test(name, classId, tables) values ('total', 1, 'XXXXXXXXX')
go
select * from test;

insert into results(testId, peopleId, tables, eval) values (1, 2, '  X      ', '                  123123123                                                      ')
insert into results(testId, peopleId, tables, eval) values (1, 2, '  X      ', '                  111223311                                                      ')
insert into results(testId, peopleId, tables, eval) values (1, 3, '  X      ', '                  330001211                                                      ')

insert into results(testId, peopleId, tables, eval) values (2, 1, 'XXXX XX X', '111111111222222222333333333222222222000000000111111111222222222000000000333333333')
insert into results(testId, peopleId, tables, eval) values (2, 2, 'XXXXXXXXX', '333333333333333333333333333333333333333333333333333333333333333333333333333333333')
insert into results(testId, peopleId, tables, eval) values (2, 3, 'XXX XXXXX', '232323202111111111222222222000000000333333333322121212331011111333333333111111111')

select * from results where testId = 1;

select r.peopleId as peopleId, p.name as peopleName, r.testId as testId, c.id as classId, r.tables as tables, r.eval as eval, r.date as date from results r join people p on r.peopleId = p.id join class c on c.id = p.classId;
delete from results;
select * from people
update people set type = 'P' where id = 4;
