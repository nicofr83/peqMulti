﻿namespace peqMulti
{
    partial class peqStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picLeft = new System.Windows.Forms.PictureBox();
            this.picRight = new System.Windows.Forms.PictureBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.prBar = new System.Windows.Forms.ProgressBar();
            this.btOk = new System.Windows.Forms.Button();
            this.txLeft = new System.Windows.Forms.TextBox();
            this.txRight = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btQuit = new System.Windows.Forms.Button();
            this.btPause = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // picLeft
            // 
            this.picLeft.Location = new System.Drawing.Point(38, 149);
            this.picLeft.Name = "picLeft";
            this.picLeft.Size = new System.Drawing.Size(312, 251);
            this.picLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLeft.TabIndex = 0;
            this.picLeft.TabStop = false;
            // 
            // picRight
            // 
            this.picRight.Location = new System.Drawing.Point(514, 149);
            this.picRight.Name = "picRight";
            this.picRight.Size = new System.Drawing.Size(312, 251);
            this.picRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRight.TabIndex = 1;
            this.picRight.TabStop = false;
            // 
            // txtResult
            // 
            this.txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResult.Location = new System.Drawing.Point(393, 442);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(80, 80);
            this.txtResult.TabIndex = 3;
            // 
            // prBar
            // 
            this.prBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.prBar.Location = new System.Drawing.Point(0, 0);
            this.prBar.Name = "prBar";
            this.prBar.Size = new System.Drawing.Size(874, 31);
            this.prBar.TabIndex = 4;
            // 
            // btOk
            // 
            this.btOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOk.Location = new System.Drawing.Point(736, 519);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(90, 30);
            this.btOk.TabIndex = 5;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // txLeft
            // 
            this.txLeft.BackColor = System.Drawing.SystemColors.Control;
            this.txLeft.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txLeft.Location = new System.Drawing.Point(150, 54);
            this.txLeft.Name = "txLeft";
            this.txLeft.Size = new System.Drawing.Size(80, 73);
            this.txLeft.TabIndex = 6;
            this.txLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txRight
            // 
            this.txRight.BackColor = System.Drawing.SystemColors.Control;
            this.txRight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txRight.Location = new System.Drawing.Point(625, 54);
            this.txRight.Name = "txRight";
            this.txRight.Size = new System.Drawing.Size(80, 73);
            this.txRight.TabIndex = 7;
            this.txRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtInfo
            // 
            this.txtInfo.BackColor = System.Drawing.SystemColors.Control;
            this.txtInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInfo.Location = new System.Drawing.Point(81, 570);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(788, 13);
            this.txtInfo.TabIndex = 8;
            this.txtInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::peqMulti.Properties.Resources.peqMultiLogo;
            this.pictureBox1.Location = new System.Drawing.Point(356, 198);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(152, 157);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // btQuit
            // 
            this.btQuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btQuit.Location = new System.Drawing.Point(729, 423);
            this.btQuit.Name = "btQuit";
            this.btQuit.Size = new System.Drawing.Size(97, 34);
            this.btQuit.TabIndex = 11;
            this.btQuit.Text = "Quit";
            this.btQuit.UseVisualStyleBackColor = true;
            this.btQuit.Click += new System.EventHandler(this.btQuit_Click);
            // 
            // btPause
            // 
            this.btPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPause.Location = new System.Drawing.Point(608, 423);
            this.btPause.Name = "btPause";
            this.btPause.Size = new System.Drawing.Size(97, 34);
            this.btPause.TabIndex = 10;
            this.btPause.Text = "Pause";
            this.btPause.UseVisualStyleBackColor = true;
            this.btPause.Click += new System.EventHandler(this.btPause_Click);
            // 
            // peqStudent
            // 
            this.AcceptButton = this.btOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 587);
            this.Controls.Add(this.btQuit);
            this.Controls.Add(this.btPause);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.txRight);
            this.Controls.Add(this.txLeft);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.prBar);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.picRight);
            this.Controls.Add(this.picLeft);
            this.Name = "peqStudent";
            this.Text = "peqStudent";
            ((System.ComponentModel.ISupportInitialize)(this.picLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picLeft;
        private System.Windows.Forms.PictureBox picRight;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.ProgressBar prBar;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.TextBox txLeft;
        private System.Windows.Forms.TextBox txRight;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btQuit;
        private System.Windows.Forms.Button btPause;
    }
}