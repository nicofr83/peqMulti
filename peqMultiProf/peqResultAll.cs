﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace peqMulti
{
    public partial class peqResultAll : Form
    {
        resultType typeResult = resultType.NONE;
        aggregationOption agregOption;

        public peqResultAll()
        {
            InitializeComponent();
        }
        private void loadCb1(resultType reTp)
        {
            switch(reTp)
            {
                case resultType.CLASSTABLE:
                case resultType.CLASSSTUDENT:
                    label1.Text = "Class";
                    cb1.DataSource = classes.me.dataSource();
                    break;
                case resultType.TESTSTUDENT:
                case resultType.TESTTABLE:
                    label1.Text = "Test";
                    cb1.DataSource = tests.me.dataSource();
                    break;
            }
        }
        private void initCb1(resultType reTp)
        {
            typeResult = reTp;
            label1.Visible = false;
            label2.Visible = false;
            cb1.Visible = false;
            cb2.Visible = false;
            switch (reTp)
            {                
                case resultType.TESTTABLE:
                    agregOption = aggregationOption.STATONETABLE;
                    cbClassStudent.Checked = false;
                    cbClassTable.Checked = false;
                    cbTestStudent.Checked = false;
                    break;

                case resultType.TESTSTUDENT:
                    agregOption = aggregationOption.SUMALLTABLES;
                    cbClassStudent.Checked = false;
                    cbClassTable.Checked = false;
                    cbTestTable.Checked = false;
                    break;
                case resultType.CLASSTABLE:
                    agregOption = aggregationOption.STATONETABLE;
                    cbClassStudent.Checked = false;
                    cbTestStudent.Checked = false;
                    cbTestTable.Checked = false;
                    break;
                case resultType.CLASSSTUDENT:
                    agregOption = aggregationOption.SUMALLTABLES;
                    cbClassTable.Checked = false;
                    cbTestStudent.Checked = false;
                    cbTestTable.Checked = false;
                    break;
            }
            loadCb1(reTp);
            label1.Visible = true;
            cb1.Visible = true;
        }
        private void loadCb2()
        {
            switch (typeResult)
            {
                case resultType.CLASSTABLE:
                case resultType.TESTTABLE:
                    label2.Text = "table";
                    cb2.DataSource = allTables.me.dataSource();
                    break;
                case resultType.TESTSTUDENT:
                case resultType.CLASSSTUDENT:
                    label2.Text = "Student";
                    cb2.DataSource = people.me.dataSourceStudents();
                    break;
            }
        }
        private void updateAggregData()
        {
            switch (typeResult)
            {
                case resultType.TESTTABLE:
                    if (cb2.Text.Length == 0)
                        agregOption = aggregationOption.SPLITPERTABLE;
                    else
                        agregOption = aggregationOption.STATONETABLE;
                    break;

                case resultType.TESTSTUDENT:
                    if (cb2.Text.Length == 0)
                        agregOption = aggregationOption.SUMALLTABLES;
                    else
                        agregOption = aggregationOption.SPLITPERTABLE;
                    break;
                case resultType.CLASSTABLE:
                    if (cb2.Text.Length == 0)
                        agregOption = aggregationOption.SPLITPERTABLE;
                    else
                        agregOption = aggregationOption.STATONETABLE;
                    break;
                case resultType.CLASSSTUDENT:
                    if (cb2.Text.Length == 0)
                        agregOption = aggregationOption.SUMALLTABLES;
                    else
                        agregOption = aggregationOption.SPLITPERTABLE;
                    break;
            }
        }
        private void initCb2()
        {
            label2.Visible = false;
            cb2.Visible = false;
            updateAggregData();

            loadCb2();

            label2.Visible = true;
            cb2.Visible = true;
        }
        private void peqResultAll_Load(object sender, EventArgs e)
        {
            cb1.Visible = false;
            cb2.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            cbYes.Checked = true;
            cbOnGoing.Checked = false;
            cbNO.Checked = false;
            resultAll.me.loadFromDB();
        }

        enum col1Type { TABLE = 1, STUDENT = 2};
        private void compute()
        {
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            resultAll.me.loadFromDB();
        }

        private void cbTestTable_CheckedChanged(object sender, EventArgs e)
        {
            initCb1(resultType.TESTTABLE);
        }

        private void cbTestStudent_CheckedChanged(object sender, EventArgs e)
        {
            initCb1(resultType.TESTSTUDENT);
        }

        private void cbClassTable_CheckedChanged(object sender, EventArgs e)
        {
            initCb1(resultType.CLASSTABLE);
        }

        private void cbClassStudent_CheckedChanged(object sender, EventArgs e)
        {
            initCb1(resultType.CLASSSTUDENT);
        }

        private void cb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb1.Text.Length == 0) {
                cb2.Visible = false;
                label2.Visible = false;
                return;
            }
            initCb2();
        }

        private void btCompute_Click(object sender, EventArgs e)
        {
            String className = null;
            String peopleName = null;
            String tableName = null;
            String testName = null;
            String tables = "XXXXXXXXX";
            test tt;

            updateAggregData();

            switch (typeResult)
            {
                case resultType.TESTTABLE:
                    testName = cb1.Text;
                    if (cb2.Text.Length > 0)
                        tableName = cb2.Text;
                    tt = tests.me.get(testName);
                    tables = tt.tables;
                    break;
                case resultType.TESTSTUDENT:
                    testName = cb1.Text;
                    if (cb2.Text.Length > 0)
                        peopleName = cb2.Text;
                    tt = tests.me.get(testName);
                    tables = tt.tables;
                    break;
                case resultType.CLASSTABLE:
                    className = cb1.Text;
                    if (cb2.Text.Length > 0)
                        tableName = cb2.Text;
                    break;
                case resultType.CLASSSTUDENT:
                    className = cb1.Text;
                    if (cb2.Text.Length > 0)
                        peopleName = cb2.Text;
                    break;
            }
            dgResultAll.DataSource = null;
            dgResultAll.Rows.Clear();
            resultAll.me.compute(className, peopleName, tableName, testName, tables, agregOption, typeResult, cbYes.Checked, cbOnGoing.Checked, cbNO.Checked);
            dgResultAll.DataSource = dispResults.me.dataSource();
            dgResultAll.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;
            dgResultAll.Columns[0].DefaultCellStyle.Font = new Font("Arial", 16F, GraphicsUnit.Pixel);
            for (Int32 idx = 0; idx < 9; idx++)
            {
                dgResultAll.Columns[idx + 1].HeaderText = resultAll.me.colHdr[idx];
                dgResultAll.Columns[idx + 1].SortMode = DataGridViewColumnSortMode.Automatic;
                dgResultAll.Columns[idx + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgResultAll.Columns[idx + 1].DefaultCellStyle.Font = new Font("Arial", 16F, GraphicsUnit.Pixel);

            }
        }

        private void cb2_SelectedIndexChanged(object sender, EventArgs e)
        {
            btCompute_Click(sender, e);
        }

        private void cbOnGoing_CheckedChanged(object sender, EventArgs e)
        {
            btCompute_Click(sender, e);
        }

        private void cbYes_CheckedChanged(object sender, EventArgs e)
        {
            btCompute_Click(sender, e);
        }

        private void cbNO_CheckedChanged(object sender, EventArgs e)
        {
            btCompute_Click(sender, e);
        }

        private void btPause_Click(object sender, EventArgs e)
        {
            
        }
    }
}
