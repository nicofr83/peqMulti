﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using peqMulti;

namespace peqMultiProf
{
    public partial class peqTest : Form
    {
        private classe myClass;
        private test curTest;
        public peqTest(String className)
        {
            InitializeComponent();
            myClass = classes.me.get(className);
            if (myClass == null)
                throw new Exception("Classe " + className + " not found !");
        }

        private void peqTest_Load(object sender, EventArgs e)
        {
            cbClass.DataSource = classes.me.dataSource();
            dgTest.DataSource = tests.me.getDataSourceForAClass(myClass.id);
            dgTest.ReadOnly = true;
            desactivateAllButtons();
        }
        private void resetForm (Boolean bRO)
        {
            txName.ReadOnly = bRO;
            txName.Text = "";
            dtDate.Value = DateTime.Now;
            cbClass.Text = myClass.name;
            cbt1.Checked = false;
            cbt2.Checked = false;
            cbt3.Checked = false;
            cbt4.Checked = false;
            cbt5.Checked = false;
            cbt6.Checked = false;
            cbt7.Checked = false;
            cbt8.Checked = false;
            cbt9.Checked = false;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            resetForm(false);
            activateSaveButton();
        }
        private void activateSaveButton()
        {
            resetForm(false);
            btDelete.Visible = false;
            btUpd.Visible = false;
            btSave.Visible = true;
        }
        private void activateUpdateButton(Boolean bDelete = false)
        {
            resetForm(false);
            btDelete.Visible = bDelete;
            btUpd.Visible = true;
            btSave.Visible = false;
        }
        private void desactivateAllButtons()
        {
            resetForm(true);
            btDelete.Visible = false;
            btUpd.Visible = false;
            btSave.Visible = false;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            string tables = "";
            if (cbt1.Checked) tables += "X"; else tables += " ";
            if (cbt2.Checked) tables += "X"; else tables += " ";
            if (cbt3.Checked) tables += "X"; else tables += " ";
            if (cbt4.Checked) tables += "X"; else tables += " ";
            if (cbt5.Checked) tables += "X"; else tables += " ";
            if (cbt6.Checked) tables += "X"; else tables += " ";
            if (cbt7.Checked) tables += "X"; else tables += " ";
            if (cbt8.Checked) tables += "X"; else tables += " ";
            if (cbt9.Checked) tables += "X"; else tables += " ";
            tests.me.addSql(txName.Text, dtDate.Value, cbClass.Text, tables, 0);
            dgTest.DataSource = null;
            dgTest.Rows.Clear();
            dgTest.DataSource = tests.me.getDataSourceForAClass(myClass.id);
            desactivateAllButtons();
        }

        private void dgTest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            String tstName = "";
            try
            {
                tstName = dgTest["name", e.RowIndex].Value.ToString();
            } catch { return; }
            curTest = tests.me.get(tstName);
            activateUpdateButton();
            txName.Text = curTest.name;
            dtDate.Value = curTest.date;
            if (curTest.tables.Substring(0, 1) == "X") cbt1.Checked = true; else cbt1.Checked = false;
            if (curTest.tables.Substring(1, 1) == "X") cbt2.Checked = true; else cbt2.Checked = false;
            if (curTest.tables.Substring(2, 1) == "X") cbt3.Checked = true; else cbt3.Checked = false;
            if (curTest.tables.Substring(3, 1) == "X") cbt4.Checked = true; else cbt4.Checked = false;
            if (curTest.tables.Substring(4, 1) == "X") cbt5.Checked = true; else cbt5.Checked = false;
            if (curTest.tables.Substring(5, 1) == "X") cbt6.Checked = true; else cbt6.Checked = false;
            if (curTest.tables.Substring(6, 1) == "X") cbt7.Checked = true; else cbt7.Checked = false;
            if (curTest.tables.Substring(7, 1) == "X") cbt8.Checked = true; else cbt8.Checked = false;
            if (curTest.tables.Substring(8, 1) == "X") cbt9.Checked = true; else cbt9.Checked = false;
            cbClass.Text = myClass.name;
        }

        private void btUpd_Click(object sender, EventArgs e)
        {
            desactivateAllButtons();
            curTest.name = txName.Text;
            curTest.date = dtDate.Value;
            string tables = "";
            if (cbt1.Checked) tables += "X"; else tables += " ";
            if (cbt2.Checked) tables += "X"; else tables += " ";
            if (cbt3.Checked) tables += "X"; else tables += " ";
            if (cbt4.Checked) tables += "X"; else tables += " ";
            if (cbt5.Checked) tables += "X"; else tables += " ";
            if (cbt6.Checked) tables += "X"; else tables += " ";
            if (cbt7.Checked) tables += "X"; else tables += " ";
            if (cbt8.Checked) tables += "X"; else tables += " ";
            if (cbt9.Checked) tables += "X"; else tables += " ";
            curTest.updateSql();
            dgTest.DataSource = null;
            dgTest.Rows.Clear();
            dgTest.DataSource = tests.me.getDataSourceForAClass(myClass.id);
        }
    }
}
