﻿namespace peqMulti
{
    partial class peqTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbClass = new System.Windows.Forms.ComboBox();
            this.cbPeople = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btTest = new System.Windows.Forms.Button();
            this.btStudent = new System.Windows.Forms.Button();
            this.btParameters = new System.Windows.Forms.Button();
            this.btReports = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // cbClass
            // 
            this.cbClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(326, 50);
            this.cbClass.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(762, 63);
            this.cbClass.TabIndex = 0;
            this.cbClass.SelectedIndexChanged += new System.EventHandler(this.cbClass_SelectedIndexChanged);
            // 
            // cbPeople
            // 
            this.cbPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPeople.FormattingEnabled = true;
            this.cbPeople.Location = new System.Drawing.Point(326, 139);
            this.cbPeople.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cbPeople.Name = "cbPeople";
            this.cbPeople.Size = new System.Drawing.Size(762, 63);
            this.cbPeople.TabIndex = 1;
            this.cbPeople.SelectedIndexChanged += new System.EventHandler(this.cbPeople_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(170, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 55);
            this.label1.TabIndex = 2;
            this.label1.Text = "Class";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(164, 138);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 55);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name";
            // 
            // btTest
            // 
            this.btTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTest.Location = new System.Drawing.Point(32, 251);
            this.btTest.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btTest.Name = "btTest";
            this.btTest.Size = new System.Drawing.Size(155, 65);
            this.btTest.TabIndex = 6;
            this.btTest.Text = "Test";
            this.btTest.UseVisualStyleBackColor = true;
            this.btTest.Visible = false;
            this.btTest.Click += new System.EventHandler(this.btTest_Click);
            // 
            // btStudent
            // 
            this.btStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStudent.Location = new System.Drawing.Point(291, 251);
            this.btStudent.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btStudent.Name = "btStudent";
            this.btStudent.Size = new System.Drawing.Size(200, 65);
            this.btStudent.TabIndex = 7;
            this.btStudent.Text = "Student";
            this.btStudent.UseVisualStyleBackColor = true;
            this.btStudent.Visible = false;
            this.btStudent.Click += new System.EventHandler(this.btStudent_Click);
            // 
            // btParameters
            // 
            this.btParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btParameters.Location = new System.Drawing.Point(912, 251);
            this.btParameters.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btParameters.Name = "btParameters";
            this.btParameters.Size = new System.Drawing.Size(286, 65);
            this.btParameters.TabIndex = 8;
            this.btParameters.Text = "Parameters";
            this.btParameters.UseVisualStyleBackColor = true;
            this.btParameters.Visible = false;
            this.btParameters.Click += new System.EventHandler(this.btParameters_Click);
            // 
            // btReports
            // 
            this.btReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btReports.Location = new System.Drawing.Point(595, 251);
            this.btReports.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btReports.Name = "btReports";
            this.btReports.Size = new System.Drawing.Size(213, 65);
            this.btReports.TabIndex = 9;
            this.btReports.Text = "Reports";
            this.btReports.UseVisualStyleBackColor = true;
            this.btReports.Visible = false;
            this.btReports.Click += new System.EventHandler(this.btReports_Click);
            // 
            // peqTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 371);
            this.Controls.Add(this.btReports);
            this.Controls.Add(this.btParameters);
            this.Controls.Add(this.btStudent);
            this.Controls.Add(this.btTest);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPeople);
            this.Controls.Add(this.cbClass);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "peqTeacher";
            this.Text = "PEQ - Multiplication - TEACHERS";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbClass;
        private System.Windows.Forms.ComboBox cbPeople;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btTest;
        private System.Windows.Forms.Button btStudent;
        private System.Windows.Forms.Button btParameters;
        private System.Windows.Forms.Button btReports;
    }
}

