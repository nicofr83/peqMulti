﻿namespace peqMulti
{
    partial class peqResultAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgResultAll = new System.Windows.Forms.DataGridView();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb2 = new System.Windows.Forms.ComboBox();
            this.cb1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbClassStudent = new System.Windows.Forms.CheckBox();
            this.cbClassTable = new System.Windows.Forms.CheckBox();
            this.cbOnGoing = new System.Windows.Forms.CheckBox();
            this.cbYes = new System.Windows.Forms.CheckBox();
            this.cbNO = new System.Windows.Forms.CheckBox();
            this.cbTestStudent = new System.Windows.Forms.CheckBox();
            this.cbTestTable = new System.Windows.Forms.CheckBox();
            this.btRefresh = new System.Windows.Forms.Button();
            this.btCompute = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgResultAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgResultAll
            // 
            this.dgResultAll.AllowUserToOrderColumns = true;
            this.dgResultAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResultAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgResultAll.Location = new System.Drawing.Point(0, 0);
            this.dgResultAll.Name = "dgResultAll";
            this.dgResultAll.Size = new System.Drawing.Size(1168, 501);
            this.dgResultAll.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btRefresh);
            this.splitContainer1.Panel2.Controls.Add(this.btCompute);
            this.splitContainer1.Size = new System.Drawing.Size(1168, 735);
            this.splitContainer1.SplitterDistance = 656;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.cb2);
            this.splitContainer2.Panel1.Controls.Add(this.cb1);
            this.splitContainer2.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgResultAll);
            this.splitContainer2.Size = new System.Drawing.Size(1168, 656);
            this.splitContainer2.SplitterDistance = 151;
            this.splitContainer2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(309, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(309, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // cb2
            // 
            this.cb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb2.FormattingEnabled = true;
            this.cb2.Location = new System.Drawing.Point(375, 108);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(532, 28);
            this.cb2.TabIndex = 2;
            this.cb2.SelectedIndexChanged += new System.EventHandler(this.cb2_SelectedIndexChanged);
            // 
            // cb1
            // 
            this.cb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb1.FormattingEnabled = true;
            this.cb1.Location = new System.Drawing.Point(375, 67);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(532, 28);
            this.cb1.TabIndex = 1;
            this.cb1.SelectedIndexChanged += new System.EventHandler(this.cb1_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbClassStudent);
            this.panel1.Controls.Add(this.cbClassTable);
            this.panel1.Controls.Add(this.cbOnGoing);
            this.panel1.Controls.Add(this.cbYes);
            this.panel1.Controls.Add(this.cbNO);
            this.panel1.Controls.Add(this.cbTestStudent);
            this.panel1.Controls.Add(this.cbTestTable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1168, 54);
            this.panel1.TabIndex = 0;
            // 
            // cbClassStudent
            // 
            this.cbClassStudent.AutoSize = true;
            this.cbClassStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClassStudent.Location = new System.Drawing.Point(535, 12);
            this.cbClassStudent.Name = "cbClassStudent";
            this.cbClassStudent.Size = new System.Drawing.Size(149, 24);
            this.cbClassStudent.TabIndex = 6;
            this.cbClassStudent.Text = "by Class-Student";
            this.cbClassStudent.UseVisualStyleBackColor = true;
            this.cbClassStudent.CheckedChanged += new System.EventHandler(this.cbClassStudent_CheckedChanged);
            // 
            // cbClassTable
            // 
            this.cbClassTable.AutoSize = true;
            this.cbClassTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClassTable.Location = new System.Drawing.Point(365, 12);
            this.cbClassTable.Name = "cbClassTable";
            this.cbClassTable.Size = new System.Drawing.Size(131, 24);
            this.cbClassTable.TabIndex = 5;
            this.cbClassTable.Text = "by Class-Table";
            this.cbClassTable.UseVisualStyleBackColor = true;
            this.cbClassTable.CheckedChanged += new System.EventHandler(this.cbClassTable_CheckedChanged);
            // 
            // cbOnGoing
            // 
            this.cbOnGoing.AutoSize = true;
            this.cbOnGoing.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOnGoing.Location = new System.Drawing.Point(954, 8);
            this.cbOnGoing.Name = "cbOnGoing";
            this.cbOnGoing.Size = new System.Drawing.Size(106, 28);
            this.cbOnGoing.TabIndex = 4;
            this.cbOnGoing.Text = "OnGoing";
            this.cbOnGoing.UseVisualStyleBackColor = true;
            this.cbOnGoing.CheckedChanged += new System.EventHandler(this.cbOnGoing_CheckedChanged);
            // 
            // cbYes
            // 
            this.cbYes.AutoSize = true;
            this.cbYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbYes.Location = new System.Drawing.Point(1073, 8);
            this.cbYes.Name = "cbYes";
            this.cbYes.Size = new System.Drawing.Size(61, 28);
            this.cbYes.TabIndex = 3;
            this.cbYes.Text = "Yes";
            this.cbYes.UseVisualStyleBackColor = true;
            this.cbYes.CheckedChanged += new System.EventHandler(this.cbYes_CheckedChanged);
            // 
            // cbNO
            // 
            this.cbNO.AutoSize = true;
            this.cbNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNO.Location = new System.Drawing.Point(887, 8);
            this.cbNO.Name = "cbNO";
            this.cbNO.Size = new System.Drawing.Size(54, 28);
            this.cbNO.TabIndex = 2;
            this.cbNO.Text = "No";
            this.cbNO.UseVisualStyleBackColor = true;
            this.cbNO.CheckedChanged += new System.EventHandler(this.cbNO_CheckedChanged);
            // 
            // cbTestStudent
            // 
            this.cbTestStudent.AutoSize = true;
            this.cbTestStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTestStudent.Location = new System.Drawing.Point(185, 12);
            this.cbTestStudent.Name = "cbTestStudent";
            this.cbTestStudent.Size = new System.Drawing.Size(141, 24);
            this.cbTestStudent.TabIndex = 1;
            this.cbTestStudent.Text = "by Test-Student";
            this.cbTestStudent.UseVisualStyleBackColor = true;
            this.cbTestStudent.CheckedChanged += new System.EventHandler(this.cbTestStudent_CheckedChanged);
            // 
            // cbTestTable
            // 
            this.cbTestTable.AutoSize = true;
            this.cbTestTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTestTable.Location = new System.Drawing.Point(23, 12);
            this.cbTestTable.Name = "cbTestTable";
            this.cbTestTable.Size = new System.Drawing.Size(123, 24);
            this.cbTestTable.TabIndex = 0;
            this.cbTestTable.Text = "by Test-Table";
            this.cbTestTable.UseVisualStyleBackColor = true;
            this.cbTestTable.CheckedChanged += new System.EventHandler(this.cbTestTable_CheckedChanged);
            // 
            // btRefresh
            // 
            this.btRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRefresh.Location = new System.Drawing.Point(834, 18);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(107, 34);
            this.btRefresh.TabIndex = 5;
            this.btRefresh.Text = "Refresh Results";
            this.btRefresh.UseVisualStyleBackColor = true;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // btCompute
            // 
            this.btCompute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCompute.Location = new System.Drawing.Point(998, 19);
            this.btCompute.Name = "btCompute";
            this.btCompute.Size = new System.Drawing.Size(114, 36);
            this.btCompute.TabIndex = 0;
            this.btCompute.Text = "Compute";
            this.btCompute.UseVisualStyleBackColor = true;
            this.btCompute.Click += new System.EventHandler(this.btCompute_Click);
            // 
            // peqResultAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 735);
            this.Controls.Add(this.splitContainer1);
            this.Name = "peqResultAll";
            this.Text = "peqResultAll";
            this.Load += new System.EventHandler(this.peqResultAll_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgResultAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView dgResultAll;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btCompute;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ComboBox cb2;
        private System.Windows.Forms.ComboBox cb1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cbNO;
        private System.Windows.Forms.CheckBox cbTestStudent;
        private System.Windows.Forms.CheckBox cbTestTable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbOnGoing;
        private System.Windows.Forms.CheckBox cbYes;
        private System.Windows.Forms.Button btRefresh;
        private System.Windows.Forms.CheckBox cbClassStudent;
        private System.Windows.Forms.CheckBox cbClassTable;
    }
}