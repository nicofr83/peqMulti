﻿namespace peqMultiProf
{
    partial class peqTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.txName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClass = new System.Windows.Forms.ComboBox();
            this.cbt1 = new System.Windows.Forms.CheckBox();
            this.cbt2 = new System.Windows.Forms.CheckBox();
            this.cbt9 = new System.Windows.Forms.CheckBox();
            this.cbt8 = new System.Windows.Forms.CheckBox();
            this.cbt5 = new System.Windows.Forms.CheckBox();
            this.cbt7 = new System.Windows.Forms.CheckBox();
            this.cbt6 = new System.Windows.Forms.CheckBox();
            this.cbt4 = new System.Windows.Forms.CheckBox();
            this.cbt3 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btDelete = new System.Windows.Forms.Button();
            this.btUpd = new System.Windows.Forms.Button();
            this.dgTest = new System.Windows.Forms.DataGridView();
            this.btAdd = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTest)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgTest);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btSave);
            this.splitContainer1.Panel2.Controls.Add(this.btAdd);
            this.splitContainer1.Panel2.Controls.Add(this.btUpd);
            this.splitContainer1.Panel2.Controls.Add(this.btDelete);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.cbClass);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.dtDate);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.txName);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(1481, 781);
            this.splitContainer1.SplitterDistance = 614;
            this.splitContainer1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(130, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txName
            // 
            this.txName.Location = new System.Drawing.Point(171, 28);
            this.txName.Name = "txName";
            this.txName.Size = new System.Drawing.Size(257, 20);
            this.txName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(490, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(526, 28);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(215, 20);
            this.dtDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(815, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Class";
            // 
            // cbClass
            // 
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(853, 28);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(132, 21);
            this.cbClass.TabIndex = 5;
            // 
            // cbt1
            // 
            this.cbt1.AutoSize = true;
            this.cbt1.Location = new System.Drawing.Point(15, 25);
            this.cbt1.Name = "cbt1";
            this.cbt1.Size = new System.Drawing.Size(62, 17);
            this.cbt1.TabIndex = 6;
            this.cbt1.Text = "Table 1";
            this.cbt1.UseVisualStyleBackColor = true;
            // 
            // cbt2
            // 
            this.cbt2.AutoSize = true;
            this.cbt2.Location = new System.Drawing.Point(104, 25);
            this.cbt2.Name = "cbt2";
            this.cbt2.Size = new System.Drawing.Size(62, 17);
            this.cbt2.TabIndex = 7;
            this.cbt2.Text = "Table 2";
            this.cbt2.UseVisualStyleBackColor = true;
            // 
            // cbt9
            // 
            this.cbt9.AutoSize = true;
            this.cbt9.Location = new System.Drawing.Point(727, 25);
            this.cbt9.Name = "cbt9";
            this.cbt9.Size = new System.Drawing.Size(62, 17);
            this.cbt9.TabIndex = 8;
            this.cbt9.Text = "Table 9";
            this.cbt9.UseVisualStyleBackColor = true;
            // 
            // cbt8
            // 
            this.cbt8.AutoSize = true;
            this.cbt8.Location = new System.Drawing.Point(638, 25);
            this.cbt8.Name = "cbt8";
            this.cbt8.Size = new System.Drawing.Size(62, 17);
            this.cbt8.TabIndex = 9;
            this.cbt8.Text = "Table 8";
            this.cbt8.UseVisualStyleBackColor = true;
            // 
            // cbt5
            // 
            this.cbt5.AutoSize = true;
            this.cbt5.Location = new System.Drawing.Point(371, 25);
            this.cbt5.Name = "cbt5";
            this.cbt5.Size = new System.Drawing.Size(62, 17);
            this.cbt5.TabIndex = 10;
            this.cbt5.Text = "Table 5";
            this.cbt5.UseVisualStyleBackColor = true;
            // 
            // cbt7
            // 
            this.cbt7.AutoSize = true;
            this.cbt7.Location = new System.Drawing.Point(549, 25);
            this.cbt7.Name = "cbt7";
            this.cbt7.Size = new System.Drawing.Size(62, 17);
            this.cbt7.TabIndex = 11;
            this.cbt7.Text = "Table 7";
            this.cbt7.UseVisualStyleBackColor = true;
            // 
            // cbt6
            // 
            this.cbt6.AutoSize = true;
            this.cbt6.Location = new System.Drawing.Point(460, 25);
            this.cbt6.Name = "cbt6";
            this.cbt6.Size = new System.Drawing.Size(62, 17);
            this.cbt6.TabIndex = 12;
            this.cbt6.Text = "Table 6";
            this.cbt6.UseVisualStyleBackColor = true;
            // 
            // cbt4
            // 
            this.cbt4.AutoSize = true;
            this.cbt4.Location = new System.Drawing.Point(282, 25);
            this.cbt4.Name = "cbt4";
            this.cbt4.Size = new System.Drawing.Size(62, 17);
            this.cbt4.TabIndex = 13;
            this.cbt4.Text = "Table 4";
            this.cbt4.UseVisualStyleBackColor = true;
            // 
            // cbt3
            // 
            this.cbt3.AutoSize = true;
            this.cbt3.Location = new System.Drawing.Point(193, 25);
            this.cbt3.Name = "cbt3";
            this.cbt3.Size = new System.Drawing.Size(62, 17);
            this.cbt3.TabIndex = 14;
            this.cbt3.Text = "Table 3";
            this.cbt3.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbt2);
            this.panel1.Controls.Add(this.cbt3);
            this.panel1.Controls.Add(this.cbt1);
            this.panel1.Controls.Add(this.cbt4);
            this.panel1.Controls.Add(this.cbt9);
            this.panel1.Controls.Add(this.cbt6);
            this.panel1.Controls.Add(this.cbt8);
            this.panel1.Controls.Add(this.cbt7);
            this.panel1.Controls.Add(this.cbt5);
            this.panel1.Location = new System.Drawing.Point(171, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(814, 76);
            this.panel1.TabIndex = 15;
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(1022, 12);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(117, 36);
            this.btDelete.TabIndex = 16;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            // 
            // btUpd
            // 
            this.btUpd.Location = new System.Drawing.Point(1022, 60);
            this.btUpd.Name = "btUpd";
            this.btUpd.Size = new System.Drawing.Size(117, 36);
            this.btUpd.TabIndex = 18;
            this.btUpd.Text = "Update";
            this.btUpd.UseVisualStyleBackColor = true;
            this.btUpd.Click += new System.EventHandler(this.btUpd_Click);
            // 
            // dgTest
            // 
            this.dgTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTest.Location = new System.Drawing.Point(0, 0);
            this.dgTest.Name = "dgTest";
            this.dgTest.Size = new System.Drawing.Size(1481, 614);
            this.dgTest.TabIndex = 0;
            this.dgTest.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTest_CellContentClick);
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Location = new System.Drawing.Point(12, 20);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(77, 76);
            this.btAdd.TabIndex = 19;
            this.btAdd.Text = "+";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(1022, 110);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(117, 36);
            this.btSave.TabIndex = 20;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // peqTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1481, 781);
            this.Controls.Add(this.splitContainer1);
            this.Name = "peqTest";
            this.Text = "peqTest";
            this.Load += new System.EventHandler(this.peqTest_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgTest;
        private System.Windows.Forms.Button btUpd;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cbt2;
        private System.Windows.Forms.CheckBox cbt3;
        private System.Windows.Forms.CheckBox cbt1;
        private System.Windows.Forms.CheckBox cbt4;
        private System.Windows.Forms.CheckBox cbt9;
        private System.Windows.Forms.CheckBox cbt6;
        private System.Windows.Forms.CheckBox cbt8;
        private System.Windows.Forms.CheckBox cbt7;
        private System.Windows.Forms.CheckBox cbt5;
        private System.Windows.Forms.ComboBox cbClass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btSave;
    }
}